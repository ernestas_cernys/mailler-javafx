/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.entities;

import com.cernys.testfx.exceptions.EntryException;

/**
 *
 * @author CDA-25
 */
public class Message {

//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Attributes de la classe-----------------------------  
  private String fileName;
  private Address sender;
  private Address recipient;
  private String subject;
  private String messageText;

//------------Getters et Setters des Attributes de la classe--------------------  
//--------------------------Methodes de la classe-------------------------------  
//--------------------------Attributes d’instance-------------------------------  
//--------------------------Constructeurs--------------------------------------- 
  public Message (){
    
  }
  
  /**
   *
   * @param sender
   * @param recipient
   * @param subject
   * @param message
   * @throws EntryException
   */
  public Message(Address sender, Address recipient, String subject, String message) throws EntryException {
    setSender(sender);
    setRecipient(recipient);
    setSubject(subject);
    setMessageText(message);
  }
//--------------------------Methodes d’instance public-------------------------- 
//--------------------------Methodes d’instance protected----------------------- 
//--------------------------Methodes d’instance private------------------------- 

  private void testNull(Object object, String attributName) throws EntryException {
    if (object == null) {
      throw new EntryException(attributName + " can't be null");
    }
  }
//--------------------------Methodes d’instance abstract------------------------ 
//-------------Gettters et Setters des Attributes d’instance--------------------   

  /**
   * @return the sender
   */
  public Address getSender() {
    return sender;
  }

  /**
   * @return the recipient
   */
  public Address getRecipient() {
    return recipient;
  }

  /**
   * @return the subject
   */
  public String getSubject() {
    return subject;
  }

  /**
   * @return the messageText
   */
  public String getMessageText() {
    return messageText;
  }

  /**
   * @param sender the sender to set
   * @throws com.cernys.testfx.exceptions.EntryException
   */
  public void setSender(Address sender) throws EntryException {
    testNull(sender, "Sender");
    this.sender = sender;
  }

  /**
   * @param recipient the recipient to set
   * @throws com.cernys.testfx.exceptions.EntryException
   */
  public void setRecipient(Address recipient) throws EntryException {
    testNull(recipient, "Recipient");
    this.recipient = recipient;
  }

  /**
   * @param subject the subject to set
   */
  public void setSubject(String subject) {
    if (subject != null && subject.trim().isEmpty()) {
      subject = "Sans objet";
    }
    this.subject = subject;
  }

  /**
   * @param messageText the text of Message to set
   */
  public void setMessageText(String messageText) {
    if (messageText != null && messageText.trim().isEmpty()) {
      messageText = null;
    }
    this.messageText = messageText;
  }

  /**
   * @return the fileName
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * @param fileName
   * @throws com.cernys.testfx.exceptions.EntryException
   */
  public void setFileName(String fileName) throws EntryException {
    if (fileName != null && fileName.trim().isEmpty()) {
      throw new EntryException("Message Name can't be empty");
    }
    this.fileName = fileName;
  }

}
