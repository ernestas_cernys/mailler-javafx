/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.entities;

import com.cernys.testfx.exceptions.EntryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public class Address {
//----------------------------Logger--------------------------------------------

  private static final Logger LOGGER = LogManager.getLogger(Address.class.getName());

//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Attributes de la classe-----------------------------  
//------------Getters et Setters des Attributes de la classe--------------------  
//--------------------------Methodes de la classe-------------------------------  
//--------------------------Attributes d’instance-------------------------------  
  private String fullName;
  private String emailAddress;
//--------------------------Constructeurs---------------------------------------  

  /**
   * Empty constructor  
   */
  public Address() {

  }

  /**
   * Constructor
   * @param fullName
   * @param address
   * @throws EntryException
   */
  public Address(String fullName, String address) throws EntryException {
    setFullName(fullName);
    setEmailAddress(emailAddress);
  }
//--------------------------Methodes d’instance public-------------------------- 

  @Override
  public String toString() {
    return this.getFullName() + "," + this.getEmailAddress();
  }
  
//--------------------------Methodes d’instance protected----------------------- 
//--------------------------Methodes d’instance private------------------------- 

  private void testNullAndEmpty(String pEntryString, String attributName) throws EntryException {
    if (pEntryString == null || pEntryString.trim().equals("")) {
      throw new EntryException(attributName + " can't be empty (recieved:\""
              + pEntryString + "\")");
    }
  }
//--------------------------Methodes d’instance abstract------------------------ 
//-------------Gettters et Setters des Attributes d’instance--------------------   

  /**
   * @return the fullName
   */
  public String getFullName() {
    return fullName;
  }

  /**
   * @return the emailAddress
   */
  public String getEmailAddress() {
    return emailAddress;
  }

  /**
   * @param fullName the fullName to set
   * @throws com.cernys.testfx.exceptions.EntryException
   */
  public void setFullName(String fullName) throws EntryException {
    testNullAndEmpty(fullName, "Full name");
    this.fullName = fullName;
  }

  /**
   * @param emailAddress the emailAddress to set
   * @throws com.cernys.testfx.exceptions.EntryException
   */
  public void setEmailAddress(String emailAddress) throws EntryException {
    testNullAndEmpty(emailAddress, "Email Address");
    this.emailAddress = emailAddress;
  }

}
