/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cernys.testfx.entities;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author CDA-25
 */
public class MailMannager {
//----------------------------Logger--------------------------------------------

  private static final Logger LOGGER = LogManager.getLogger(MailMannager.class.getName());

//--------------------------CONSTANTS-------------------------------------------  
//--------------------------Attributes de la classe----------------------------- 
  private static List <Message> messages = new ArrayList<>();
  private static List <Address> addresses = new ArrayList<>();
//------------Getters et Setters des Attributes de la classe--------------------  
//--------------------------Methodes de la classe-------------------------------  
  public static void sendMessage (Message message){
    saveMessage(message);
  }
  
  public static void saveMessage (Message message){
    addMessage(message);
  }
  
  public static void addMessage (Message message){
    messages.add(message);
  }
  
  public static void showAllMessages (){
    for (Message m : messages) {
      m.toString();
    }
  }
//--------------------------Attributes d’instance-------------------------------  
//--------------------------Constructeurs---------------------------------------  
//--------------------------Methodes d’instance public-------------------------- 
//--------------------------Methodes d’instance protected----------------------- 
//--------------------------Methodes d’instance private------------------------- 
//--------------------------Methodes d’instance abstract------------------------ 
//-------------Gettters et Setters des Attributes d’instance--------------------   

}
