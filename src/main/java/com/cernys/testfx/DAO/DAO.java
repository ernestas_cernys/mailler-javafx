/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.DAO;

import com.cernys.testfx.exceptions.DAOException;
import java.util.Map;

/**
 *
 * @author CDA-25
 * @param <T>
 */
public interface DAO<T> {

  public Map findAll() throws DAOException;

  public T find(String s) throws DAOException;

  public boolean save(T object) throws DAOException;

  public boolean delete(T object) throws DAOException;
}
