/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.DAO;

import static com.cernys.testfx.DAO.DAOLocalFile.readData;
import com.cernys.testfx.entities.Address;
import com.cernys.testfx.exceptions.DAOException;
import com.cernys.testfx.exceptions.EntryException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CDA-25
 */
public class DAOAddress extends DAOLocalFile implements DAO<Address> {

  private static final String DIRECTORY_NAME = "AddressBook";
  private static final String FILE_NAME = "text.txt";
  private static Map<String, Address> addresses = new HashMap<>();

  @Override
  public Map findAll() throws DAOException {
    for (String s : readData(DIRECTORY_NAME, FILE_NAME)) {
      try {
        String[] string = s.split(",");
        Address a = new Address();
        a.setFullName(string[0]);
        a.setEmailAddress(string[1]);
        addresses.put(string[1], a);
      } catch (EntryException ex) {
        throw new DAOException("Error while setting data from AddressBook");
      }
    }
    return addresses;
  }

  @Override
  public Address find(String s) throws DAOException {
    if (addresses == null || addresses.isEmpty()) {
      findAll();
    }
    if (addresses.get(s) == null) {
      throw new DAOException("Address not found in AddressBook");
    }
    return addresses.get(s);
  }

  @Override
  public boolean save(Address address) throws DAOException {
    addresses.put(address.getEmailAddress(), address);
    writeToFile(DIRECTORY_NAME, FILE_NAME, address.toString(), true);
    return true;
  }

  @Override
  public boolean delete(Address address) throws DAOException {
    if (addresses == null || addresses.isEmpty()) {
      findAll();
    }
    if (addresses.get(address.getEmailAddress()) == null) {
      throw new DAOException("Address not found in AddressBook");
    }
    addresses.remove(address.getEmailAddress());
    overwriteAll(DIRECTORY_NAME, FILE_NAME, (List) addresses.values());
    return true;
  }
}
