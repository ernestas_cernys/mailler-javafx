/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.DAO;

import com.cernys.testfx.exceptions.DAOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author CDA-25
 */
public abstract class DAOLocalFile {

  private static final Logger LOGGER = LogManager.getLogger(DAOLocalFile.class.getName());

  /**
   *
   * @param directoryName
   * @param fileName
   * @return
   * @throws DAOException
   */
  private static void createDirectoryAndFile(String directoryName, String fileName) throws DAOException {
    Path path = null;
    try {
      path = Paths.get(System.getProperty("user.home"), directoryName);
      if (!Files.exists(path)) {
        Files.createDirectory(path);
      }
      path = Paths.get(path.toString(), fileName);
      if (!Files.exists(path)) {
        Files.createFile(path);
      }
    } catch (IOException ioe) {
      LOGGER.fatal("Error while creating: " + path);
      LOGGER.fatal("Message: " + ioe.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(ioe.getStackTrace()));
      throw new DAOException("Error while creating: " + path);
    }
  }

  /**
   *
   * @param directoryName
   * @param fileName
   * @return
   * @throws DAOException
   */
  protected static List<String> readData(String directoryName, String fileName) throws DAOException {
    createDirectoryAndFile(directoryName, fileName);
    Path path = Paths.get(System.getProperty("user.home"), directoryName, fileName);
    try (BufferedReader reader
            = new BufferedReader(
                    new FileReader(path.toFile()))) {
      String result = reader.readLine();
      List<String> results = new ArrayList<>();
      while (result != null) {
        results.add(result);
      }
      return results;
    } catch (NullPointerException | IOException npe) {
      LOGGER.fatal("File not found: " + fileName);
      LOGGER.fatal("Message: " + npe.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(npe.getStackTrace()));
      throw new DAOException("File not found: " + fileName);
    }
  }

  /**
   *
   * @param directoryName
   * @param fileName
   * @param string
   * @param append
   * @throws DAOException
   */
  protected static void writeToFile(String directoryName, String fileName, String string, boolean append) throws DAOException {
    Path path = Paths.get(System.getProperty("user.home"), directoryName, fileName);
    try (BufferedWriter out = new BufferedWriter(new FileWriter(path.toFile(), append))) {
      out.write(string);
    } catch (IOException ex) {
      LOGGER.fatal("File not found: " + fileName);
      LOGGER.fatal("Message: " + ex.getMessage());
      LOGGER.fatal("StackTrace: " + Arrays.toString(ex.getStackTrace()));
      throw new DAOException("File not found: " + fileName);
    }
  }

  ///////////////////// only write to file needed? 
  /**
   *
   * @param directoryName
   * @param fileName
   * @param strings
   * @throws DAOException
   */
  protected static void overwriteAll(String directoryName, String fileName, List strings) throws DAOException {
    StringBuilder builder = new StringBuilder();
    for (Object o : strings) {
      builder.append(o.toString()).append(System.lineSeparator());
    }
    writeToFile(directoryName, fileName, builder.toString(), false);
  }

}
