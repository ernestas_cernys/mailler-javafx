/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cernys.testfx.DAO;

import static com.cernys.testfx.DAO.DAOLocalFile.readData;
import com.cernys.testfx.entities.Message;
import com.cernys.testfx.exceptions.DAOException;
import com.cernys.testfx.exceptions.EntryException;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author CDA-25
 */
public class DAOMessage extends DAOLocalFile implements DAO<Message> {

  private static final String DIRECTORY_NAME = "Messages";
  private static final String FILE_NAME = "messageList";
  private static Map<String, Message> messages = new HashMap<>();

  @Override
  public Map findAll() throws DAOException {
    List<String> fileNames = readData(DIRECTORY_NAME, FILE_NAME);
    for (String fileName : fileNames) {
      try {
        List<String> messageLines = readData(DIRECTORY_NAME, fileName);
        Message m = new Message();
        // set data with empty Address data 
        m.setFileName(messageLines.get(0));
        m.setSubject(messageLines.get(3));
        m.setMessageText(messageLines.get(4));
        messages.put(fileName, m);
      } catch (EntryException ex) {
        throw new DAOException("Error while setting message data");
      }
    }
    return messages;
  }

  @Override
  public Message find(String fileName) throws DAOException {
    Message message = new Message();
    try {
      List<String> messageLines = readData(DIRECTORY_NAME, fileName);
      message.setFileName(messageLines.get(0));
      message.setSender(new DAOAddress().find(messageLines.get(1)));
      message.setRecipient(new DAOAddress().find(messageLines.get(2)));
      message.setSubject(messageLines.get(3));
      message.setMessageText(messageLines.get(4));
    } catch (EntryException ex) {
      throw new DAOException("Error while setting message data");
    }
    return message;
  }

  @Override
  public boolean save(Message message) throws DAOException {
    String messageString
            = message.getFileName() + System.lineSeparator()
            + message.getSender().getEmailAddress() + System.lineSeparator()
            + message.getRecipient().getEmailAddress() + System.lineSeparator()
            + message.getSubject() + System.lineSeparator()
            + message.getMessageText();

    writeToFile(DIRECTORY_NAME, message.getFileName(), messageString, false);
    writeToFile(DIRECTORY_NAME, FILE_NAME, message.getFileName(), true);
    return true;
  }

  @Override
  public boolean delete(Message message) throws DAOException {
    Path path = Paths.get(System.getProperty("user.home"), DIRECTORY_NAME, message.getFileName());
    File file = path.toFile();
    file.delete();
    if (messages == null || messages.isEmpty()) {
      findAll();
    }
    if (messages.get(message.getFileName()) == null) {
      throw new DAOException("Message not found in MessageList");
    }
    messages.remove(message.getFileName());
    overwriteAll(DIRECTORY_NAME, FILE_NAME, (List) messages.values());
    return true;
  }

}
