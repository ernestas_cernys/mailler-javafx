package com.cernys.testfx;

import com.cernys.testfx.DAO.DAOAddress;
import com.cernys.testfx.entities.Address;
import com.cernys.testfx.entities.MailMannager;
import com.cernys.testfx.entities.Message;
import com.cernys.testfx.exceptions.DAOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.Callback;
import javax.swing.JOptionPane;

public class FXMLController implements Initializable {

  @FXML
  private MenuBar MenuBar;

  @FXML
  private Menu MenuMessage;

  @FXML
  private Menu MenuOptions;

  @FXML
  private Menu MenuHelp;

  @FXML
  private Button buttonCreate;

  @FXML
  private Button buttonOpen;

  @FXML
  private Button buttonSend;

  @FXML
  private Label labelSubject;

  @FXML
  private TextField textFieldSubject;

  @FXML
  private Label labelRecipient;

  @FXML
  private ComboBox<Address> comboBoxAddress;

  @FXML
  private TextArea textAreaMessage;

  @FXML
  void getMessage(ActionEvent event) {

  }

  @FXML
  void newMessage(ActionEvent event) {
    Object[] options = {"Oui", "Non"};
    int n = JOptionPane.showOptionDialog(null, "Voulez-vous commencer nouveau message?",
            "", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    if (n == 0) {
      textFieldSubject.setText("");
      textAreaMessage.setText("");

    }
  }

  @FXML
  void sendMessage(ActionEvent event) {
    Message message = new Message();

    MailMannager.sendMessage(message);
  }

  @FXML
  void setComboBox() {
    comboBoxAddress = new ComboBox<Address>();
    Map addresses = new HashMap();
    try {
      addresses = new DAOAddress().findAll();
    } catch (DAOException ex) {
      JOptionPane.showMessageDialog(null, "Erreur de lecture des fichiers" + ex.getMessage(), "", JOptionPane.INFORMATION_MESSAGE);
    }
    List<Address> list = new ArrayList<>();
    list.addAll(addresses.values());
    ObservableList<Address> addressList = FXCollections.observableList(list);
    comboBoxAddress.setItems(addressList);
    comboBoxAddress.setCellFactory(new Callback<ListView<Address>, ListCell<Address>>() {

      @Override
      public ListCell<Address> call(ListView<Address> arg0) {
        ListCell<Address> cell = new ListCell<Address>() {

          @Override
          public void updateItem(Address address, boolean empty) {
            super.updateItem(address, empty);
            if (address != null) {
              setText(address.toString());
            } else {
              setText(null);
            }
          }
        };
        return cell;
      }
    });

  }

  @Override
  public void initialize(URL url, ResourceBundle rb) {
    try {
      new DAOAddress().findAll();
//  setComboBox();
    } catch (DAOException ex) {
      Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
